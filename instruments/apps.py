from django.apps import AppConfig


class InstrumentsConfig(AppConfig):
    name = 'instruments'

    def ready(self, *args, **kwargs):
        from . import signals
