from django.conf import settings
from django.db import models
from django.urls import reverse


class UOMe(models.Model):
    owed_to = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="debts_owed_me", on_delete=models.PROTECT
    )
    owed_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="debts_i_owe", on_delete=models.PROTECT
    )
    description = models.TextField()
    cash_value = models.DecimalField(max_digits=12, decimal_places=2)
    acknowledged_at = models.DateTimeField(null=True, blank=True)

    def get_absolute_url(self):
        return reverse("uome_read", kwargs={"pk": self.pk})


class ActionChoices(models.IntegerChoices):
    CREATE = (1, "Create UOMe")
    ACKNOWLEDGE = (2, "Acknowledge UOMe")


class Notification(models.Model):
    recipient = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.PROTECT
    )
    actor = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.PROTECT
    )
    target = models.ForeignKey(UOMe, on_delete=models.CASCADE)
    action = models.IntegerField(choices=ActionChoices.choices)
