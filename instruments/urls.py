from django.urls import path

from .views import UOMeAcknowledgeView, UOMeCreateView, UOMeReadView

urlpatterns = [
    path("uome/new/", UOMeCreateView.as_view(), name="uome_create"),
    path("uome/<int:pk>", UOMeReadView.as_view(), name="uome_read"),
    path(
        "uome/<int:pk>/acknowledge",
        UOMeAcknowledgeView.as_view(),
        name="uome_acknowledge",
    ),
]
