from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.views.generic import CreateView, DetailView

from .forms import UOMeCreateForm
from .models import UOMe


class UOMeCreateView(LoginRequiredMixin, CreateView):
    model = UOMe
    form_class = UOMeCreateForm
    template_name = "instruments/uome_create.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class UOMeReadView(LoginRequiredMixin, DetailView):
    model = UOMe
    template_name = "instruments/uome_read.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if self.request.user not in [self.object.owed_by, self.object.owed_to]:
            raise PermissionDenied

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class UOMeAcknowledgeView(LoginRequiredMixin, DetailView):
    model = UOMe
    template_name = "instruments/uome_acknowledge.html"
    _is_uome_already_acknowledged = False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if self.object.owed_by != self.request.user:
            raise PermissionDenied

        context = self.get_context_data(object=self.object)

        if self.object.acknowledged_at is not None:
            self._is_uome_already_acknowledged = True
            return self.render_to_response(context)

        self.object.acknowledged_at = timezone.now()
        self.object.save(update_fields=["acknowledged_at"])
        return self.render_to_response(context)

    def get_template_names(self):
        if self._is_uome_already_acknowledged:
            return ["instruments/uome_acknowledge_failed.html"]
        return super().get_template_names()
