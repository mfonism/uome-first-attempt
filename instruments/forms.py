from django import forms

from .models import UOMe


class UOMeCreateForm(forms.ModelForm):
    class Meta:
        model = UOMe
        fields = ["owed_by", "description", "cash_value"]

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.owed_to = self.request.user
        if commit:
            instance.save()
        return instance
