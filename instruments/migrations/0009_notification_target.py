# Generated by Django 3.1.3 on 2021-02-03 10:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('instruments', '0008_auto_20210203_1124'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='target',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='instruments.uome'),
            preserve_default=False,
        ),
    ]
