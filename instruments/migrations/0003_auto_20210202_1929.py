# Generated by Django 3.1.3 on 2021-02-02 18:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('instruments', '0002_uome_owed_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='uome',
            name='owed_by',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='debts_i_owe', to='auth.user'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='uome',
            name='owed_to',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='debts_owed_me', to=settings.AUTH_USER_MODEL),
        ),
    ]
