import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from instruments.models import ActionChoices, Notification, UOMe


class TestUOMeAcknowledgeView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.creditor = get_user_model().objects.create_user(
            username="grey", password="grey", email="grey@gmail.com"
        )
        cls.debtor = get_user_model().objects.create_user(
            username="kiwi", password="kiwi", email="kiwi@gmail.com"
        )

        cls.uome = UOMe.objects.create(
            owed_to=cls.creditor,
            owed_by=cls.debtor,
            description="Two pairs of shorts - navy and pink, NGN5000 each",
            cash_value=10_000,
        )
        cls.url = reverse("uome_acknowledge", kwargs={"pk": cls.uome.pk})

    def test_unauthenticated_user_cannot_acknowledge(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_cannot_acknowledge_uome_if_not_debtor(self):
        self.client.force_login(user=self.creditor)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 403)

    def test_acknowledge_uome_as_debtor(self):
        self.client.force_login(user=self.debtor)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.uome.refresh_from_db()
        self.assertIsNotNone(self.uome.acknowledged_at)
        self.assertTemplateUsed(resp, "instruments/uome_acknowledge.html")
        self.assertContains(resp, "successfully acknowledged")
        self.assertContains(resp, reverse("uome_read", kwargs={"pk": self.uome.pk}))

    def test_debtor_cannot_acknowledge_uome_more_than_once(self):
        ten_seconds_ago = timezone.now() - datetime.timedelta(seconds=10)
        self.uome.acknowledged_at = ten_seconds_ago
        self.uome.save()

        self.client.force_login(user=self.debtor)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.uome.refresh_from_db()
        self.assertEqual(self.uome.acknowledged_at, ten_seconds_ago)
        self.assertTemplateUsed(resp, "instruments/uome_acknowledge_failed.html")
        self.assertContains(resp, "already acknowledged this UOMe")
        self.assertContains(resp, reverse("uome_read", kwargs={"pk": self.uome.pk}))

    def test_notification_is_created_for_creditor(self):
        old_notification_count = Notification.objects.count()

        self.client.force_login(user=self.debtor)
        self.client.get(self.url)

        self.assertEqual(Notification.objects.count(), old_notification_count + 1)
        created_notification = Notification.objects.last()
        self.assertEqual(created_notification.recipient, self.creditor)
        self.assertEqual(created_notification.actor, self.debtor)
        self.assertEqual(created_notification.target, self.uome)
        self.assertEqual(created_notification.action, ActionChoices.ACKNOWLEDGE)
