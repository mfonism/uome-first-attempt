from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from instruments.models import ActionChoices, Notification, UOMe


class TestUOMeCreateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.grey = get_user_model().objects.create_user(
            username="grey", password="grey", email="grey@aol.com"
        )
        cls.kiwi = get_user_model().objects.create_user(
            username="kiwi", password="kiwi", email="kiwi@aol.com"
        )

        cls.url = reverse("uome_create")

        cls.payload = {
            "owed_by": cls.kiwi.pk,
            "description": "A pair of shorts, Navy and pink - NGN 5000 each.",
            "cash_value": 10_000,
        }

    def test_unauthenticated_user_cannot_get_creation_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_get_creation_page(self):
        self.client.force_login(user=self.grey)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, "instruments/uome_create.html")
        self.assertContains(resp, "Create UOMe")
        self.assertNotContains(resp, "owed_to")
        self.assertContains(resp, "owed_by")
        self.assertContains(resp, "description")
        self.assertContains(resp, "cash_value")

    def test_unauthenticated_user_cannot_create(self):
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(resp.status_code, 302)

    def test_create(self):
        old_uome_count = UOMe.objects.count()

        self.client.force_login(user=self.grey)
        resp = self.client.post(self.url, self.payload)

        self.assertEqual(UOMe.objects.count(), old_uome_count + 1)
        created_uome = UOMe.objects.last()
        self.assertRedirects(resp, reverse("uome_read", kwargs={"pk": created_uome.pk}))

    def test_created_uome(self):
        self.client.force_login(user=self.grey)
        self.client.post(self.url, self.payload)
        created_uome = UOMe.objects.last()

        self.assertEqual(created_uome.owed_to, self.grey)
        self.assertEqual(created_uome.owed_by, self.kiwi)
        self.assertEqual(created_uome.description, self.payload["description"])
        self.assertEqual(created_uome.cash_value, self.payload["cash_value"])

    def test_notification_is_created_for_debtor(self):
        old_notification_count = Notification.objects.count()

        self.client.force_login(user=self.grey)
        self.client.post(self.url, self.payload)

        self.assertEqual(Notification.objects.count(), old_notification_count + 1)
        notification = Notification.objects.last()
        self.assertEqual(notification.recipient, self.kiwi)
        self.assertEqual(notification.actor, self.grey)
        self.assertEqual(notification.target, UOMe.objects.last())
        self.assertEqual(notification.action, ActionChoices.CREATE)
