from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from instruments.models import UOMe


class TestUOMeReadView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.creditor = get_user_model().objects.create_user(
            username="grey", password="grey", email="grey@aol.com"
        )
        cls.debtor = get_user_model().objects.create_user(
            username="kiwi", password="kiwi", email="kiwi@aol"
        )

        cls.uome = UOMe.objects.create(
            owed_to=cls.creditor,
            owed_by=cls.debtor,
            description="Two pairs of shorts, navy and pink. NGN5000 each.",
            cash_value=10_000,
        )

        cls.url = reverse("uome_read", kwargs={"pk": cls.uome.pk})

    def test_unauthenticated_user_cannot_get_read_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 302)

    def test_cannot_read_page_if_not_creditor_or_debtor(self):
        kish = get_user_model().objects.create_user(
            username="kish", password="kish", email="kish@aol.com"
        )

        self.client.force_login(user=kish)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 403)

    def test_read_uome(self):
        self.client.force_login(user=self.creditor)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, "instruments/uome_read.html")
        self.assertContains(resp, self.uome.description)
        self.assertContains(resp, self.uome.cash_value)

    def test_uome_is_described_correctly_to_logged_in_creditor(self):
        self.client.force_login(user=self.creditor)
        resp = self.client.get(self.url)

        self.assertContains(resp, f"{self.debtor.username} owes you")
        self.assertNotContains(resp, f"you owe {self.creditor.username}")

    def test_uome_is_described_correctly_to_logged_in_debtor(self):
        self.client.force_login(user=self.debtor)
        resp = self.client.get(self.url)

        self.assertContains(resp, f"you owe {self.creditor.username}")
        self.assertNotContains(resp, f"{self.debtor.username} owes you")

    def test_debtor_sees_uome_acknowledgement_link(self):
        self.client.force_login(user=self.debtor)
        resp = self.client.get(self.url)

        self.assertContains(resp, "Acknowledge UOMe")
        self.assertContains(
            resp, reverse("uome_acknowledge", kwargs={"pk": self.uome.pk})
        )

    def test_creditor_does_not_see_acknowledgement_link(self):
        self.client.force_login(user=self.creditor)
        resp = self.client.get(self.url)

        self.assertNotContains(resp, "Acknowledge UOMe")
        self.assertNotContains(
            resp, reverse("uome_acknowledge", kwargs={"pk": self.uome.pk})
        )
