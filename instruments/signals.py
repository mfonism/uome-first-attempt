from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import ActionChoices, Notification, UOMe


@receiver(post_save, sender=UOMe)
def notify_debtor_of_created_uome(sender, instance, created, **kwargs):
    if not created:
        return
    Notification.objects.create(
        recipient=instance.owed_by,
        actor=instance.owed_to,
        target=instance,
        action=ActionChoices.CREATE,
    )


@receiver(post_save, sender=UOMe)
def notify_creditor_of_acknowledged_uome(
    sender, instance, created, update_fields, **kwargs
):
    if update_fields is None:
        return

    if not (len(update_fields) == 1 and "acknowledged_at" in update_fields):
        return

    Notification.objects.create(
        recipient=instance.owed_to,
        actor=instance.owed_by,
        target=instance,
        action=ActionChoices.ACKNOWLEDGE,
    )
