* Prevent user from READing a `UOMe` if they are neither creditor or
  debtor in the context of the `UOMe`.

* Prevent user from creating `UOMe`s to self by removing them from the choices/options
  they are presented by the `owed_by` field of the form.

  __HINT:__

  _`ForeignKey` fields on models map to `ModelChoiceField`s in model forms._
  _These `ModelChoiceField`s have a `queryset` attribute which is by default populated_
  _with all the objects of the corresponding model._

  _You should filter this in the `init` method of the form class._
